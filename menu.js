var menu = $('#menu');

class RouteManager {
    static findRoutesWithOriginAndDestination(origin_tile_id, destination_tile_id) {
        var routes = [];
        $.each(global_data, function (index, row) {
            if (row.origin === origin_tile_id && row.destination === destination_tile_id) routes = routes.concat(row.routes);
        });
        return routes;
    }
}

class MenuBuilder {

    static buildRoutesMenu(routes) {
        $.each(routes, function (index, route) {
            menu.append(
                route.getHTML()
            );
        });
    }

    static buildSelectProfilesMenu() {
        menu.empty();
        menu.append('<div class="row">');
        $.each(global_profiles, function (index, profile) {

            var profileActive = profileController.contains(profile);
            var profileDiv = $('<div class="col m6"></div>');
            profileDiv.append(profile.getJQueryHTML(profileActive));
            profileDiv.click(function () {
                profileController.clickProfile(profile);
            });
            menu.append(profileDiv)
        });
        menu.append('</div>');

    }
}

class MenuController {

    static selectProfiles() {
        menu.empty();
        MenuBuilder.buildSelectProfilesMenu();
    }

    static showRoutes(destination_tile_id) {
        menu.empty();
        var origin_tile_id = global_tiles.T14;
        var routes = RouteManager.findRoutesWithOriginAndDestination(origin_tile_id, destination_tile_id);
        MenuBuilder.buildRoutesMenu(routes);
    }
}
