const global_tiles = {
    T0: 'tile_0',
    T1: 'tile_1',
    T2: 'tile_2',
    T3: 'tile_3',
    T4: 'tile_4',
    T5: 'tile_5',
    T6: 'tile_6',
    T7: 'tile_7',
    T8: 'tile_8',
    T9: 'tile_9',
    T10: 'tile_10',
    T11: 'tile_11',
    T12: 'tile_12',
    T13: 'tile_12',
    T14: 'tile_14'
};

const global_profiles = {
    SHOPPING: new Profile('SHOPPING','./img/profiles/SHOPPING.png'),
    PMR: new Profile('PMR', './img/profiles/PMR.png'),
    LEISURE: new Profile('LEISURE', './img/profiles/OCIO_NOCTURNO.png'),
    TOURISM: new Profile('TOURISM', './img/profiles/TURISTA.png'),
    COMER: new Profile('COMER', './img/profiles/COMER.png'),
    EVENTOS: new Profile('EVENTOS', './img/profiles/EVENTOS.png'),
    OCIO: new Profile('OCIO', './img/profiles/OCIO.png'),
    PLAYA: new Profile('PLAYA', './img/profiles/PLAYA.png')
};

const global_lines = {
    METRO_L1: new Line('L1', 'img/lines/METRO_L1.png'),
    METRO_L2: new Line('L2', 'img/lines/METRO_L2.png'),
    METRO_L3: new Line('L3', 'img/lines/METRO_L3.png'),
    METRO_L4: new Line('L4', 'img/lines/METRO_L4.png'),
    METRO_L5: new Line('L5', 'img/lines/METRO_L5.png'),
    METRO_L6: new Line('L6', 'img/lines/METRO_L6.png'),
    METRO_L7: new Line('L7', 'img/lines/METRO_L7.png'),
    RENFE_R2: new Line('R2', 'img/lines/RENFE_R2.png'),
    RENFE_R2N: new Line('R2nord', 'img/lines/r2nord.jpg'),
    RENFE_R2S: new Line('R2sud', 'img/lines/r2sud.jpg'),
    FGC_L8: new Line('L8', 'img/lines/FGC_L8.png'),
    FGC_S3: new Line('S3', 'img/lines/FGC_S3.png'),
    FGC_S4: new Line('S4', 'img/lines/FGC_S4.png'),
    FGC_S8: new Line('S8', 'img/lines/FGC_S8.png'),
    FGC_S9: new Line('S9', 'img/lines/FGC_S9.png'),
    FGC_R5: new Line('R5', 'img/lines/FGC_R5.png'),
    FGC_R6: new Line('R6', 'img/lines/FGC_R6.png'),
    FGC_R50: new Line('R50', 'img/lines/FGC_R50.png'),
    FGC_R60: new Line('R60', 'img/lines/FGC_R60.png')
};


const global_data = [
    {
        origin: global_tiles.T0,
        destination: global_tiles.T1,
        profile: global_profiles.SHOPPING,
        routes: [
            new Route(
                time = 35,
                accessibility = 0.3,
                accessibility = 0.9,
                comfort = 0.8,
                stations = [
                    new Station('Espanya', [global_lines.RENFE_R2, global_lines.METRO_L5, global_lines.METRO_L2]),
                    new Station('Z. Universitaria', [global_lines.RENFE_R2N])
                ]
            ),
            new Route(
                time = 288,
                accessibility = 0.3,
                accessibility = 0.9,
                comfort = 0.8,
                stations = [
                    new Station('Espanya', [global_lines.METRO_L1]),
                    new Station('Z. Universitaria', [global_lines.METRO_L3])
                ]
            )
        ]
    },
    {
        origin: global_tiles.T14,
        destination: global_tiles.T0,
        profile: global_profiles.TOURISM,
        routes: [
            new Route(
                time = 35,
                accessibility = 0.3,
                accessibility = 0.9,
                comfort = 0.8,
                stations = [
                    new Station('Espanya', [
                        global_lines.METRO_L1,
                        global_lines.FGC_L8,
                        global_lines.FGC_S3,
                        global_lines.FGC_S4,
                        global_lines.FGC_S8,
                        global_lines.FGC_S9,
                        global_lines.FGC_R5,
                        global_lines.FGC_R50,
                        global_lines.FGC_R6,
                        global_lines.FGC_R60
                    ]),
                    new Station('Z. Universitaria', [global_lines.METRO_L3])
                ]
            ),
            new Route(
                time = 45,
                accessibility = 0.3,
                accessibility = 0.9,
                comfort = 0.8,
                stations = [
                    new Station('Sants Estació', [
                        global_lines.RENFE_R2N,
                        global_lines.RENFE_R2S,
                        global_lines.RENFE_R2]),
                    new Station('Z. Universitaria', [global_lines.METRO_L3])
                ]
            )
        ]
    }
];



// TOURISM

const tourism_images = [
    {prefix:'tourism_layer_', url: 'iconos/9.png', id: 'campnou', coordinates: [2.12295, 41.38076], size: 0.08},
    {prefix:'tourism_layer_', url: 'iconos/10.png', id: 'montjuic', coordinates: [2.16696, 41.3629], size: 0.08},
    {prefix:'tourism_layer_', url: 'iconos/fuente.png', id: 'fuente', coordinates: [2.1517, 41.37072], size: 0.08},
    {prefix:'tourism_layer_', url: 'iconos/11.png', id: 'iglesia', coordinates: [2.0990539, 41.3599005], size: 0.08},
    {prefix:'tourism_layer_', url: 'iconos/airplane.png', id: 'airport', coordinates: [2.100775, 41.305909], size: 0.08},
];

const tourism_detail_images = [
    {prefix:'tourism_detail_', url: 'iconos/mac.jpg', id: 'mac', coordinates: [2.1262203, 41.3917334], size: 0.08},

    {prefix:'tourism_detail_', url: 'iconos/museo.png', id: 'museo1', coordinates: [2.1210711, 41.3813058], size: 0.08},
    {prefix:'tourism_detail_', url: 'iconos/museo.png', id: 'museo2', coordinates: [2.1171217, 41.388446], size: 0.08},

    {prefix:'tourism_detail_', url: 'iconos/bar.png', id: 'bar1', coordinates: [2.1309569, 41.3887634], size: 0.08},
    {prefix:'tourism_detail_', url: 'iconos/bar.png', id: 'bar2', coordinates: [2.1260103, 41.3902595], size: 0.08},

    {prefix:'tourism_detail_', url: 'iconos/banco.png', id: 'banco1', coordinates: [2.1246522, 41.3898743], size: 0.08},
    {prefix:'tourism_detail_', url: 'iconos/banco.png', id: 'banco2', coordinates: [2.1212197, 41.3899919], size: 0.08},
];

// SHOPPING

const shopping_images = [
    {prefix:'shopping_layer_', url: 'iconos/la illa.jpg', id: 'illa', coordinates: [2.1349, 41.389213], size: 0.05},
    {prefix:'shopping_layer_', url: 'iconos/corte.png', id: 'corte', coordinates: [2.128155, 41.38754], size: 0.08},
    {prefix:'shopping_layer_', url: 'iconos/hm.png', id: 'hym', coordinates: [2.13028, 41.35855], size: 0.15},
    {prefix:'shopping_layer_', url: 'iconos/desigual.jpg', id: 'desigual', coordinates: [2.07816, 41.34677], size: 0.05},
    {prefix:'shopping_layer_', url: 'iconos/diesel.jpg', id: 'diesel', coordinates: [2.134511, 41.38877], size: 0.08},
];

// STATIONS

const stations_images = [
    {prefix:'stations_layer_', url: 'iconos/metro.png', id: 'metro1', coordinates: [2.1180201, 41.3857199], size: 0.05, pmr: false},
    {prefix:'stations_layer_', url: 'iconos/metro.png', id: 'metro2', coordinates: [2.117794, 41.3859093], size: 0.05, pmr: true},
    {prefix:'stations_layer_', url: 'iconos/metro.png', id: 'metro3', coordinates: [2.1263022, 41.3878184], size: 0.05, pmr: false},
    {prefix:'stations_layer_', url: 'iconos/metro.png', id: 'metro4', coordinates: [2.126154, 41.3880307], size: 0.05, pmr: true},

    {prefix:'stations_layer_', url: 'iconos/tram.png', id: 'tram1', coordinates: [2.1265128, 41.3879658], size: 0.08, pmr: true},
    {prefix:'stations_layer_', url: 'iconos/tram.png', id: 'tram2', coordinates: [2.1215708, 41.3867143], size: 0.08, pmr: true},
    {prefix:'stations_layer_', url: 'iconos/tram.png', id: 'tram3', coordinates: [2.1184603, 41.3859271], size: 0.08, pmr: true},

    {prefix:'stations_layer_', url: 'iconos/bici.png', id: 'bici1', coordinates: [2.1256259, 41.3879968], size: 0.08, pmr: false},
    {prefix:'stations_layer_', url: 'iconos/bici.png', id: 'bici2', coordinates: [2.1236694, 41.3874901], size: 0.08, pmr: false},
    {prefix:'stations_layer_', url: 'iconos/bici.png', id: 'bici3', coordinates: [2.1231317, 41.3854812], size: 0.08, pmr: false},
    {prefix:'stations_layer_', url: 'iconos/bici.png', id: 'bici4', coordinates: [2.1288162, 41.3852879], size: 0.08, pmr: false},
];

