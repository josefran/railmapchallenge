class Route {
    constructor(time, density, accessibility, comfort, stations) {
        this.density = density;
        this.accesibility = accessibility;
        this.comfort = comfort;
        this.time = time;
        this.stations = stations;
    }

    getHTML() {
        var routeDiv = $('<div class="card-panel"></div>');
        $.each(this.stations, function (index, station) {
            routeDiv.append(station.getHTML());
        });
        return routeDiv;
    }
}

class Station {
    constructor(station_name, lines) {
        this.lines = lines;
        this.name = station_name;
    }

    getHTML() {
        var stationDiv = $('<div></div>');
        var linesRow = $('<div class="row"></div>');
        $.each(this.lines, function (index, line) {
            linesRow.append(line.getHTML());
        });
        var stationNameRow = $('<div class="row"></div>');
        var chip = $('<div class="chip"></div>');

        chip.append(this.name);
        stationNameRow.append(chip);

        stationDiv.append(linesRow);
        stationDiv.append(stationNameRow);
        return stationDiv;
    }
}

class Line {
    constructor(name, image_url) {
        this.name = name;
        this.image_url = image_url;
    }

    getHTML() {
        return '<img class=\"responsive-img\" src=\"' + this.image_url + '\" height="32" width="32" >'
    }
}

class Profile {
    constructor(name, image_url) {
        this.name = name;
        this.image_url = image_url;
    }

    getId() {
        return "PROFILE_ID_" + this.name;
    }

    getJQueryHTML(active) {
        if (active) return $(this.getHTML()).addClass("grey lighten-1");
        else return $(this.getHTML());

    }

    getHTML() {
        return '<img id = "' + this.getId() + '" class=\"circle responsive-img\" src=\"' + this.image_url + '\" style="background-color: white">'
    }
}

class ProfileController {

    constructor() {
        this.profiles = [];
    }

    activeProfile(profile) {
        var index = this.profiles.indexOf(profile);
        if (index < 0) {
            this.profiles.push(profile);
        }
    }

    deactiveProfile(profile) {
        var index = this.profiles.indexOf(profile);
        if (index > -1) {
            this.profiles.splice(index, 1);
        }
    }

    contains(profile) {
        return this.profiles.indexOf(profile) > -1;
    }

    clickProfile(profile) {
        let profileDiv = $("#" + "PROFILE_ID_" + profile.name);
        profileDiv.toggleClass("grey lighten-1");
        if (profileDiv.hasClass("grey lighten-1")) {
            profileController.activeProfile(profile);
        } else {
            profileController.deactiveProfile(profile)
        }
        MapBuilder.paintLayers();
    }


}

function activateProfileLayer(images) {
    images.forEach(img => {
        map.setLayoutProperty(img.prefix + img.id, 'visibility', 'visible');
    });
}

function deactivateProfileLayer(images) {
    images.forEach(img => {
        map.setLayoutProperty(img.prefix + img.id, 'visibility', 'none');
    });
}