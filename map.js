class MapController {
    constructor() {
        this.level = "GENERAL";
    }

    goToZoom() {
        this.level = "ZOOM";
    }

    goToGeneral() {
        this.level = "GENERAL";
    }

    isGeneral() {
        return this.level === "GENERAL";
    }

}


class MapBuilder {

    static goToGeneral() {
        mapController.goToGeneral();
        MapBuilder.paintLayers();
    }

    static goToZoom() {
        mapController.goToZoom();
        MapBuilder.paintLayers();
    }

    static paintLayers() {
        MapBuilder.showTurismLayer();
        MapBuilder.showShoppingLayer();
        MapBuilder.showStationsLayer();

    }

    static showTurismLayer() {
        if (profileController.profiles.indexOf(global_profiles.TOURISM) >= 0) {
            if (mapController.isGeneral()) {
                activateProfileLayer(tourism_images);
                deactivateProfileLayer(tourism_detail_images);

            } else {
                activateProfileLayer(tourism_images);
                activateProfileLayer(tourism_detail_images);

            }
        } else {
            deactivateProfileLayer(tourism_images);
            deactivateProfileLayer(tourism_detail_images);
        }
    }

    static showShoppingLayer() {
        if (profileController.profiles.indexOf(global_profiles.SHOPPING) >= 0) {
            if (mapController.isGeneral()) {
                activateProfileLayer(shopping_images);
            } else {
                activateProfileLayer(shopping_images);
            }
        } else {
            deactivateProfileLayer(shopping_images);
        }
    }

    static showStationsLayer() {
        if (mapController.isGeneral()) {
            deactivateProfileLayer(stations_images);
        } else {
            activateProfileLayer(stations_images);
        }
    }


}


mapboxgl.accessToken = 'pk.eyJ1IjoiYm9sb2xsbyIsImEiOiI3MDlqRnJJIn0.m-zCTI_UaEOCiCakGUDwcw';
let init_map_parameters = {
    container: 'map', // id del elemento HTML que contendrá el mapa
    style: 'mapbox://styles/mapbox/streets-v9', // Ubicación del estilo
    center: [2.11255, 41.34581], // Ubicación inicial
    zoom: 11.65, // Zoom inicial
    bearing: -45, // Ángulo de rotación inicial
    //hash: true, // Permite ir guardando la posición del mapa en la URL
    //maxBounds: [2.175, 41.39] // Constrained to the given bounds.
};
var map = new mapboxgl.Map(init_map_parameters);
var profileController = new ProfileController();
var mapController = new MapController();

// Agrega controles de navegación (zoom, rotación) al mapa:
map.addControl(new mapboxgl.NavigationControl());

var fab_return_button = $('#fab-return');
// FLOAT BUTTON
fab_return_button.click(function (e) {
    e.preventDefault();
    map.setLayoutProperty('lines', 'visibility', 'none');
    initMenuProfile();

    map.flyTo({
        center: init_map_parameters.center,
        zoom: init_map_parameters.zoom
    });
    MenuBuilder.buildSelectProfilesMenu();
    MapBuilder.goToGeneral();
});

map.on('load', function () {
    fab_return_button.hide();
    initMenuProfile();
    mapController.goToGeneral();


    map.addLayer({
        'id': 'lines',
        'type': 'line',
        'source': {
            'type': 'geojson',
            'data': './linies.geojson'
        },
        'layout': {
            'visibility': 'none'
        },
        'paint': {
            'line-color': ['get', 'stroke'],
            'line-width': 4
        }
    });

    var arrayPolygons = calculatePolygons();

    var arrayLength = arrayPolygons.length;
    for (var i = 0; i < arrayLength; i++) {
        var polygon = arrayPolygons[i];

        map.addLayer({
            'id': 'tile_' + i,
            'type': 'fill',
            'source': {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'geometry': {
                        'type': 'Polygon',
                        'coordinates': [polygon]
                    }
                }
            },
            'layout': {},
            'paint': {
                'fill-color': '#088',
                'fill-opacity': 0.2
            }
        });

        map.on('click', 'tile_' + i, function (e) {
            MapBuilder.goToZoom();

            fab_return_button.show();
            var polygon = e.features[0];
            var center = polygonCenter(polygon.geometry);

            var id = e.features[0].source;
            MenuController.showRoutes(id);

            map.flyTo({
                center: center.coordinates,
                zoom: 14.40
            });

            map.setLayoutProperty('lines', 'visibility', 'visible');
        });
    }

    tourism_images.forEach(img => {
        loadImageInLayer(img);
    });


    tourism_detail_images.forEach(img => {
        loadImageInLayer(img);
    });


    shopping_images.forEach(img => {
        loadImageInLayer(img);
    });


    stations_images.forEach(img => {
        loadImageInLayer(img);
    });

    // let circle = {
    //     prefix: 'distance_circle_',
    //     id: '1',
    //     coordinates: [2.12295, 41.38076],
    //     visibility: 'visible',
    //     paint: {
    //         "circle-radius": 10,
    //         "circle-color": "red",
    //         "circle-opacity": 0.5
    //     }
    // };
    //
    // paintCircleInLayer(circle);
});


function initMenuProfile() {
    fab_return_button.hide();
    MenuController.selectProfiles();
}

function loadImageInLayer(img) {
    map.loadImage(img.url, function (error, res) {
        map.addImage(img.id, res);
        map.addLayer({
            "id": img.prefix + img.id,
            "type": "symbol",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "FeatureCollection",
                    "features": [{
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": img.coordinates
                        }
                    }]
                }
            },
            "layout": {
                "visibility": "none",
                "icon-image": img.id,
                "icon-size": img.size
            }
        });

    });
}


function paintCircleInLayer(circle) {
    map.addLayer({
        "id": circle.prefix + circle.id,
        "type": "circle",
        "source": {
            "type": "geojson",
            "data": {
                "type": "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": circle.coordinates
                    }
                }]
            }
        },
        "layout": {
            "visibility": circle.visibility,
        },
        "paint": circle.paint
    });
}
