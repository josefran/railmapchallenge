function polygonCenter(polygon) {
    if ('geometry' in polygon) polygon = polygon.geometry;
    if (!Array.isArray(polygon)) polygon = polygon.coordinates[0];

    var minx = miny = 1000
        , maxx = maxy = -1000;
    for (var i = 0; i < polygon.length; i++) {
        var point = polygon[i];
        var x = point[0];
        var y = point[1];

        if (x < minx) minx = x;
        else if (x > maxx) maxx = x;
        if (y < miny) miny = y;
        else if (y > maxy) maxy = y;
    }

    return {
        type: 'Point',
        coordinates: [
            minx + ((maxx - minx) / 2),
            miny + ((maxy - miny) / 2)
        ]
    }
};


function calculatePolygons() {
    var coordinateInitial = [
        [2.12256, 41.37913],
        [2.11308, 41.38623],
        [2.12805, 41.39746],
        [2.13751, 41.39036],
        [2.12256, 41.37913]
    ];
    var arrayPolygons = [];
    arrayPolygons.push(coordinateInitial);
    arrayPolygons = arrayPolygons.concat(calculateColumn(coordinateInitial));

    // Right To Left
    var lastPolygon = coordinateInitial;
    for (var column = 0; column < 5; column++) {
        var polygon = leftPolygon(lastPolygon);
        arrayPolygons.push(polygon);
        lastPolygon = polygon;
        arrayPolygons = arrayPolygons.concat(calculateColumn(polygon));
    }

    return arrayPolygons;
}

function calculateColumn(initialPolygon) {
    var arrayPolygons = [];
    var lastPolygon = initialPolygon;
    for (var row = 0; row < 5; row++) {
        var polygon = bottomPolygon(lastPolygon);
        arrayPolygons.push(polygon);
        lastPolygon = polygon;
    }
    return arrayPolygons;
}

function leftPolygon(polygon) {
    var topLeftLat = polygon[1][0] - Math.abs(polygon[2][0] - polygon[1][0]);
    var topLeftLon = polygon[1][1] - Math.abs(polygon[2][1] - polygon[1][1]);
    var bottomLeftLat = polygon[0][0] - Math.abs(polygon[3][0] - polygon[0][0]);
    var bottomLeftLon = polygon[0][1] - Math.abs(polygon[3][1] - polygon[0][1]);

    return [
        [bottomLeftLat, bottomLeftLon],
        [topLeftLat, topLeftLon],
        polygon[1],
        polygon[0],
        [bottomLeftLat, bottomLeftLon]
    ];
}

function bottomPolygon(polygon) {
    var bottomLeftLat = polygon[0][0] + Math.abs(polygon[1][0] - polygon[0][0]);
    var bottomLeftLon = polygon[0][1] - Math.abs(polygon[1][1] - polygon[0][1]);
    var bottomRightLat = polygon[3][0] + Math.abs(polygon[2][0] - polygon[3][0]);
    var bottomRightLon = polygon[3][1] - Math.abs(polygon[2][1] - polygon[3][1]);

    return [
        [bottomLeftLat, bottomLeftLon],
        polygon[0],
        polygon[3],
        [bottomRightLat, bottomRightLon],
        [bottomLeftLat, bottomLeftLon]
    ];
}
